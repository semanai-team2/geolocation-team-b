from time import sleep
import RPi.GPIO as gpio

print (gpio.VERSION)

gpio.setmode(gpio.BOARD)

gpio.setup(3, gpio.IN)

gpio.setup(5, gpio.OUT)

on = False

print("Flashing LED")

for x in range(0,20):
    on = not on
    gpio.output(5, on)
    sleep(0.25)

print("Flash over")

try:
    while True:
        gpio.output(5, gpio.input(3))
except Exception as e:
    gpio.cleanup()
    print (e)
    print ("Stopping program")

gpio.cleanup()
