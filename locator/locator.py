"""
Vuelta: B9407F30-F5F8-466E-AFF9-25556B57FE6D:5383:62141 @ 0,0
Casa:   B9407F30-F5F8-466E-AFF9-25556B57FE6D:35477:54860  @ 8,20
CIT:    B9407F30-F5F8-466E-AFF9-25556B57FE6D:14790:4537 @ 5,0
"""

import blescan
import sys
import time
import os        
import RPi.GPIO as gpio
import bluetooth._bluetooth as bluez
import requests
import json
import iwparse

RSSIATONEMETER = 182
GMAPSAPIKEY = "AIzaSyDs-KwyQelYyx6sNyYmd5xGQrmmmiOarSI"
GMAPSAPIURL = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + GMAPSAPIKEY

def printInCurrentLine(str):
    print("                                         \r" + str)

def printBeacons(b, location):
    print("\033[0;0HUser Location:")  
    print(location)
    print("")

    for beacon in b:
        beacon = b[beacon]
        printInCurrentLine("%s at %s,%s" % (beacon["name"], beacon["x"], beacon["y"]))
        printInCurrentLine("RSSI: %s" % beacon["rssi"])   
        printInCurrentLine("Average: %s" % beacon["average"])

        txString = ""
        if beacon["txTimestamp"] != 0:
            txString += "Age: %s\n" % str(time.clock() - beacon["txTimestamp"])
        else:
            txString = "\n"
              
        printInCurrentLine(txString)

def debounce(channel, pullup = True):
    time.sleep(0.1)
    if pullup and not gpio.input(channel):
        return True
    elif not pullup and gpio.input(channel):
        return True
    return False

def holdForSeconds(channel, seconds, pullup = True):
    for x in range(0, seconds * 10):
          time.sleep(0.1)
          if (pullup and gpio.input(channel)) or (not pullup and not gpio.input(channel)):
              return False
    return True

def geolocate():
    body = {
        'homeMobileCountryCode': 334,
        'homeMobileNetworkCode': 20,
        'carrier': 'Telcel',
        "wifiAccessPoints": []
    }

    print("Scanning for AP...")
    rawAPData = iwparse.get_interfaces()

    for AP in rawAPData:
        body["wifiAccessPoints"].append({
            "macAddress": AP["Address"],
            "channel": AP["Channel"]
        })

    jsonPayload = json.dumps(body)
    headers = {'content-type': 'application/json'}

    print("Geolocating...")

    r = requests.post(GMAPSAPIURL, data = jsonPayload, headers = headers)

    response = json.loads(r.text)

    return response["location"]

def sendDataToFirebase(beacons, location):
    url = "https://mobileraspberrypi.firebaseio.com/panic-locations.json"

    body = {
        "location": location,
        "floor": getActiveFloor(beacons)
    }

    jsonPayload = json.dumps(body)
    headers = {'content-type': 'application/json'}

    print("Sending data to firebase...")

    r = requests.post(url, data = jsonPayload, headers = headers)

    response = json.loads(r.text)

    print(response)

def checkForPanic(beacons, location):
    if not gpio.input(3):
        if debounce(3) and holdForSeconds(3, 5):
            print("Panic!") 
            for x in range(0, 5):
                if x % 3 == 2:
                    flashLed(11, 2, 0.1)
                else:
                    flashLed(5 + 2 * (x % 3), 2, 0.1)
            sendDataToFirebase(beacons, location)
            for x in range(0, 10):
                if x % 3 == 2:
                    flashLed(11, 2, 0.1)
                else:
                    flashLed(5 + 2 * (x % 3), 2, 0.1)
            print("Panic over")
            _ = os.system('clear')

def flashLed(port, times, interval = 0.25):
    on = False
    for x in range(0,times*2):
        on = not on
        gpio.output(port, on)
        time.sleep(interval)

def outputToLeds(beacons):
    activeFloor = getActiveFloor(beacons)

    st = ""

    if activeFloor == 1:
        gpio.output(5, True)
        st = "User is in the first floor"
    else:
        gpio.output(5, False)

    if activeFloor == 2:
        gpio.output(7, True)
        st = "User is in the second floor"
    else:
        gpio.output(7, False)

    if activeFloor == 3:
        gpio.output(11, True)
        st = "User is in the third floor"
    else:
        gpio.output(11, False)

    printInCurrentLine(st)

def getActiveFloor(beacons):
    citAge = time.clock() - beacons["cit"]["txTimestamp"]
    casaAge = time.clock() - beacons["casa"]["txTimestamp"]
    vueltaAge = time.clock() - beacons["vuelta"]["txTimestamp"]
    citIntensity = 0 if type(beacons["cit"]["rssi"]) is str else beacons["cit"]["rssi"]
    casaIntensity = 0 if type(beacons["casa"]["rssi"]) is str else beacons["casa"]["rssi"]
    vueltaIntensity = 0 if type(beacons["vuelta"]["rssi"]) is str else beacons["vuelta"]["rssi"]

    if (vueltaAge < casaAge and vueltaAge < citAge) or withinRange([vueltaAge, casaAge, citAge], [vueltaIntensity, casaIntensity, citIntensity]):
        return 1
    elif (citAge < casaAge and citAge < vueltaAge) or withinRange([citAge, casaAge, vueltaAge], [citIntensity, vueltaIntensity, casaIntensity]):
        return 2
    elif (casaAge < citAge and casaAge < vueltaAge) or withinRange([casaAge, vueltaAge, citAge], [casaIntensity, vueltaIntensity, citIntensity]):
        return 3

def withinRange(ages, intensities):
    baseAge = ages[0]
    baseInt = intensities[0]

    return (baseAge - ages[1] < 5 and baseInt > intensities[1]) and (baseAge - ages[2] < 5 and baseInt > intensities[2])

if __name__ == "__main__":    
    gpio.setmode(gpio.BOARD)
    gpio.setup(3, gpio.IN)
    gpio.setup([5, 7, 11], gpio.OUT)

    location = geolocate()

    print("Testing LEDs")

    flashLed(5, 4, 0.1)
    flashLed(7, 4, 0.1)
    flashLed(11, 4, 0.1)

    dev_id = 0
    try:
        sock = bluez.hci_open_dev(dev_id)
        print("ble thread started")

    except:
        print("error accessing bluetooth device...")
        sys.exit(1)

    blescan.hci_le_set_scan_parameters(sock)
    blescan.hci_enable_le_scan(sock)

    start = time.clock()

    beacons = {
        "vuelta": {
            "x": 0,
            "y": 3,
            "name": "Beacon Vuelta",
            "rssi": "searching...",
            "txTimestamp": 0,
            "average": 0,
            "count": 0
        },
        "casa": {
            "x": 0,
            "y": 2,
            "name": "Beacon Casa",
            "rssi": "searching...",
            "txTimestamp": 0,
            "average": 0,
            "count": 0
        },
        "cit": {
            "x": 0,
            "y": 1,
            "name": "Beacon CIT",
            "rssi": "searching...",
            "txTimestamp": 0,
            "average": 0,
            "count": 0
        }
    }
    
    _ = os.system('clear')

    try:
        while True:

            returnedList = blescan.parse_events(sock, 10)
            
            for device in returnedList:
                data = device.split(",")
                udid = str(data[1])
                major = int(data[2])
                rssi = int(data[-1])
                if udid.startswith("b9407f30"):
                    if major == 5383:
                        beacons["vuelta"]["rssi"] = rssi
                        beacons["vuelta"]["txTimestamp"] = time.clock()
                        beacons["vuelta"]["count"] += 1
                        beacons["vuelta"]["average"] = ((beacons["vuelta"]["count"] - 1) * beacons["vuelta"]["average"] + beacons["vuelta"]["rssi"]) / beacons["vuelta"]["count"]
                    elif major == 14790:
                        beacons["cit"]["rssi"] = rssi
                        beacons["cit"]["txTimestamp"] = time.clock()
                        beacons["cit"]["count"] += 1
                        beacons["cit"]["average"] = ((beacons["cit"]["count"] - 1) * beacons["cit"]["average"] + beacons["cit"]["rssi"]) / beacons["cit"]["count"]
                    elif major == 35477:                
                        beacons["casa"]["rssi"] = rssi
                        beacons["casa"]["txTimestamp"] = time.clock()
                        beacons["casa"]["count"] += 1
                        beacons["casa"]["average"] = ((beacons["casa"]["count"] - 1) * beacons["casa"]["average"] + beacons["casa"]["rssi"]) / beacons["casa"]["count"]
            

            printBeacons(beacons, location)
            outputToLeds(beacons)
            checkForPanic(beacons, location)

            for b in beacons:
                if time.clock() - beacons[b]["txTimestamp"] > 50:
                    beacons[b]["rssi"] = "Searching..."
                    beacons[b]["txTimestamp"] = 0

    except Exception as e:
        gpio.cleanup()
        print ("Stopping program")
        print (e)

