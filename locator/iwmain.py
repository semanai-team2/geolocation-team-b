
import sys

from iwparse import *

def main():
    """ Pretty prints the output of iwlist scan into a table. """
    #print("1st debug")
    #parsed_cells = get_parsed_cells(sys.stdin)
    parsed_cells = get_interfaces()
    #print(parsed_cells)
    #print("2nd debug")

    

    # You can choose which columns to display here, and most importantly
    # in what order. Of course, they must exist as keys in the dict rules.
    columns = ["Name", "Address", "Quality", "Channel", "Encryption", "Signal Level"]

    #print("3rd debug")
    print_cells(parsed_cells, columns)
    #print("4th debug")

if __name__ == "__main__":
    main()