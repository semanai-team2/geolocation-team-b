function initMap() {
  let map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: {
      lat: 28.6747375,
      lng: -106.0804663
    }
  });    

  getPanicPositionsFromFirebase(map);
}

function getPanicPositionsFromFirebase(map) {
  let db = firebase.database(),
    markers = [];

  var panicRef = firebase.database().ref('panic-locations/');
  panicRef.on('value', (snapshot) => {    
    markers.forEach((m) => {
      m.setMap(null); // Remove current markers
    });

    markers = []; // De-reference current markers
  
    snapshot.forEach(function(panicLoc){
        var value = panicLoc.val();
        
        var marker = new google.maps.Marker({
            position: value.location,
            map: map,
            label: value.floor + ""
          });
    });
  });
}

firebase.initializeApp({
  apiKey: "AIzaSyBsaO2zYUMLH4-SwfUEZUB8S8hEyD1ttfg",
  authDomain: "mobileraspberrypi.firebaseapp.com",
  databaseURL: "https://mobileraspberrypi.firebaseio.com",
  projectId: "mobileraspberrypi",
  storageBucket: "mobileraspberrypi.appspot.com",
  messagingSenderId: "765072664313"
});   

/*
var marker = new google.maps.Marker({
  position: uluru,
  map: map
});
*/