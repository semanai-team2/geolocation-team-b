"""
Vuelta: B9407F30-F5F8-466E-AFF9-25556B57FE6D:5383:62141 @ 0,0
Casa:   B9407F30-F5F8-466E-AFF9-25556B57FE6D:35477:54860  @ 8,20
CIT:    B9407F30-F5F8-466E-AFF9-25556B57FE6D:14790:4537 @ 5,0
"""

import blescan
import sys
import time
import os

import bluetooth._bluetooth as bluez

RSSIATONEMETER = 182
#rssi at 20 meters: 173. what?

def printInCurrentLine(str):
    print("                                         \r" + str)

def printBeacons(b):
    print("\033[0;0H")

    for beacon in b:
        beacon = b[beacon]
        printInCurrentLine("%s at %s,%s" % (beacon["name"], beacon["x"], beacon["y"]))
        printInCurrentLine("RSSI: %s" % beacon["rssi"])   
        printInCurrentLine("Average: %s" % beacon["average"])

        txString = ""
        if beacon["txTimestamp"] != 0:
            txString += "Age: %s\n" % str(time.clock() - beacon["txTimestamp"])
        else:
            txString = "\n"
              
        printInCurrentLine(txString)


if __name__ == "__main__":
    dev_id = 0
    try:
        sock = bluez.hci_open_dev(dev_id)
        print("ble thread started")

    except:
        print("error accessing bluetooth device...")
        sys.exit(1)

    blescan.hci_le_set_scan_parameters(sock)
    blescan.hci_enable_le_scan(sock)

    start = time.clock()

    beacons = {
        "vuelta": {
            "x": 0,
            "y": 0,
            "name": "Beacon Vuelta",
            "rssi": "searching...",
            "txTimestamp": 0,
            "average": 0,
            "count": 0
        },
        "casa": {
            "x": 8,
            "y": 20,
            "name": "Beacon Casa",
            "rssi": "searching...",
            "txTimestamp": 0,
            "average": 0,
            "count": 0
        },
        "cit": {
            "x": 5,
            "y": 0,
            "name": "Beacon CIT",
            "rssi": "searching...",
            "txTimestamp": 0,
            "average": 0,
            "count": 0
        }
    }
    _ = os.system('clear')

    while True:

        returnedList = blescan.parse_events(sock, 10)
        
        for device in returnedList:
            data = device.split(",")
            udid = str(data[1])
            major = int(data[2])
            rssi = int(data[-1])
            if udid.startswith("b9407f30"):
                if major == 5383:
                    beacons["vuelta"]["rssi"] = rssi
                    beacons["vuelta"]["txTimestamp"] = time.clock()
                    beacons["vuelta"]["count"] += 1
                    beacons["vuelta"]["average"] = ((beacons["vuelta"]["count"] - 1) * beacons["vuelta"]["average"] + beacons["vuelta"]["rssi"]) / beacons["vuelta"]["count"]
                elif major == 14790:
                    beacons["cit"]["rssi"] = rssi
                    beacons["cit"]["txTimestamp"] = time.clock()
                    beacons["cit"]["count"] += 1
                    beacons["cit"]["average"] = ((beacons["cit"]["count"] - 1) * beacons["cit"]["average"] + beacons["cit"]["rssi"]) / beacons["cit"]["count"]
                elif major == 35477:                
                    beacons["casa"]["rssi"] = rssi
                    beacons["casa"]["txTimestamp"] = time.clock()
                    beacons["casa"]["count"] += 1
                    beacons["casa"]["average"] = ((beacons["casa"]["count"] - 1) * beacons["casa"]["average"] + beacons["casa"]["rssi"]) / beacons["casa"]["count"]
        

        printBeacons(beacons)