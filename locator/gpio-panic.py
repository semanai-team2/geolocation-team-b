from time import sleep

import RPi.GPIO as gpio

def debounce(channel, pullup = True):
    sleep(0.1)
    if pullup and not gpio.input(channel):
        return True
    elif not pullup and gpio.input(channel):
        return True
    return False

def holdForSeconds(channel, seconds, pullup = True):
    for x in range(0, seconds * 10):
          sleep(0.1)
          if (pullup and gpio.input(channel)) or (not pullup and not gpio.input(channel)):
              return False
    return True

def waitForPanic():
    print("Waiting for panic")
    while True:
        if not gpio.input(3):
            if debounce(3) and holdForSeconds(3, 5):
                print("Panic!") 
                flashLed(30, 0.1)
                print("Waiting for panic")

def flashLed(times, interval = 0.25):
    on = False
    for x in range(0,times*2):
        on = not on
        gpio.output(5, on)
        sleep(interval)

print (gpio.VERSION)

gpio.setmode(gpio.BOARD)

gpio.setup(3, gpio.IN)

gpio.setup(5, gpio.OUT)

flashLed(10)

try:
    waitForPanic()
except Exception as e:
    gpio.cleanup()
    print (e)
    print ("Stopping program")

gpio.cleanup()

